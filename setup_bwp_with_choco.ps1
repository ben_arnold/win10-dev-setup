###  still necesary
# # Elevate
# Get the ID and security principal of the current user account
$myWindowsID = [System.Security.Principal.WindowsIdentity]::GetCurrent()
$myWindowsPrincipal = new-object System.Security.Principal.WindowsPrincipal($myWindowsID)
# Get the security principal for the Administrator role
$adminRole = [System.Security.Principal.WindowsBuiltInRole]::Administrator
# Check to see if we are currently running "as Administrator"
if ($myWindowsPrincipal.IsInRole($adminRole)) {
    # We are running "as Administrator" - so change the title and background color to indicate this
    # $Host.UI.RawUI.WindowTitle = $myInvocation.MyCommand.Definition + "(Elevated)"
    $initial_window_title = $Host.UI.RawUI.WindowTitle
    $Host.UI.RawUI.WindowTitle = "Setup Windows Basis Workplace Client (Elevated)"
    
    # https://docs.microsoft.com/en-us/dotnet/api/system.consolecolor?redirectedfrom=MSDN&view=netframework-4.8
    $initial_background_color = $Host.UI.RawUI.BackgroundColor
    # $Host.UI.RawUI.BackgroundColor = "DarkGray"
    $Host.UI.RawUI.BackgroundColor = "Black"

    clear-host
    Write-Host "`nStart to Setup this Windows Basic Workplace Client"
}
else {
    # We are not running "as Administrator" - so relaunch as administrator
    Write-Host 'We are not running in elevated mode. Please start PowerShell again "as Administrator".'
    exit
   
    # $newProcess = new-object System.Diagnostics.ProcessStartInfo "PowerShell";
    # $newProcess.Arguments = $myInvocation.MyCommand.Definition;
    # $newProcess.Verb = "runas";
   
    # [System.Diagnostics.Process]::Start($newProcess);
   
    # # Exit from the current, unelevated, process
    # exit
}

$config_obj = [PSCustomObject]@{
    Username = ''
    DefaultDistro   = ''
    Distros  = @()
    AdditionalChoco = @()
    AdditionalPlaybooks = @()
    VSCodeExtentions = @()
    VSCodeSettings = @()
    WinGit = $null
    Docker = $null
    DockerConfig = @()
}
$step = 0

$default_config_path = 'configs.json'
$user_config_path = "~\OneDrive\$(hostname)-configs.json"

Write-Host "`n --- Step ${step}: Collect userdata"
if (Test-Path $user_config_path) {
    $config_file_path = $user_config_path
} else {
    $config_file_path = $default_config_path
}

if (Test-Path $config_file_path) {
    $json = Get-Content $config_file_path | Out-String | ConvertFrom-Json
    $config_obj.Username = $json.Username
    $config_obj.DefaultDistro = $json.DefaultDistro
    $config_obj.Distros = $json.Distros
    $config_obj.AdditionalChoco = $json.AdditionalChoco
    $config_obj.AdditionalPlaybooks = $json.AdditionalPlaybooks
    $config_obj.VSCodeExtentions = $json.VSCodeExtentions
    # $config_obj.VSCodeSettings = $json.VSCodeSettings
    $config_obj.VSCodeSettings = @{}
    $json.VSCodeSettings.psobject.properties | Foreach { $config_obj.VSCodeSettings[$_.Name] = $_.Value }

    $config_obj.WinGit = $json.WinGit

    $config_obj.Docker = $json.Docker
    $config_obj.DockerConfig = $json.DockerConfig

}

Write-Host "configs: $config_obj"
Write-Host "Please enter your informations."
if ($config_obj.Username -eq "") {
    $config_obj.Username = Read-Host "username"
}
if ($null -eq $config_obj.WinGit) {
    $bool_input = Read-Host "Do you need git in Windows? ['true','false']"
    try {
        $config_obj.WinGit = [System.Convert]::ToBoolean($bool_input) 
    } catch [FormatException] {
        $config_obj.WinGit = $false
    }
}
if ($null -eq $config_obj.Docker) {
    $bool_input = Read-Host "Do you need Docker? ['true','false']"
    try {
        $config_obj.Docker = [System.Convert]::ToBoolean($bool_input) 
    } catch [FormatException] {
        $config_obj.Docker = $false
    }
}
if ($config_obj.DockerConfig.Count -eq 0) {
    $config_obj.DockerConfig = @{} 
    $config_obj.DockerConfig['key'] = "value"

}

if ($config_obj.DefaultDistro -eq "") {

    if ($config_obj.Distros.Count -ne 0) {
        $config_obj.DefaultDistro = $config_obj.Distros | Select-Object -first 1
    } else {
        $config_obj.DefaultDistro = Read-Host "linux distro ('ubuntu1804', 'debian', 'fedora')"
    }
}

if ($config_obj.VSCodeExtentions.Count -eq 0) {
    # $config_obj.VSCodeExtentions = "ms-vscode-remote.remote-wsl", "ms-python.python", "ms-vscode.Go", "redhat.vscode-yaml", "ms-azuretools.vscode-docker", "ms-kubernetes-tools.vscode-kubernetes-tools", "vscoss.vscode-ansible", "mauve.terraform", "yzhang.markdown-all-in-one", "eamodio.gitlens", "ms-vscode.PowerShell", "Gruntfuggly.todo-tree", "ms-vscode-remote.remote-ssh" 
    $config_obj.VSCodeExtentions = "ms-vscode-remote.remote-wsl", "ms-vscode.PowerShell", "ms-vscode-remote.remote-ssh" 
}

if ($config_obj.VSCodeSettings.Count -eq 0) {
    $config_obj.VSCodeSettings = @{} 
    $config_obj.VSCodeSettings['terminal.integrated.shell.windows'] = "C:\Windows\System32\bash.exe"
    $config_obj.VSCodeSettings['files.autoSave'] = "afterDelay"
    $config_obj.VSCodeSettings['scm.providers.visible'] = 0
    $config_obj.VSCodeSettings['editor.renderWhitespace'] = "boundary"
    $config_obj.VSCodeSettings['python.pythonPath'] = "python3"
    $config_obj.VSCodeSettings['python.linting.pylintEnabled'] = $true
    $config_obj.VSCodeSettings['python.linting.enabled'] = $true
    $config_obj.VSCodeSettings['editor.formatOnSave'] = $true
    # TODO move to /home/{$distro_username}/.vscode-server/data/Machine/settings.json"
    $config_obj.VSCodeSettings['python.linting.pylintPath'] = "/usr/bin/pylint"
    $config_obj.VSCodeSettings['python.formatting.provider'] = "black"
    
    # $config_obj.VSCodeSettings[''] = ""

}


# TODO
# $shared_path_win = ''
# $shared_path_wsl = ''

$config_write_back = $false
if ($config_file_path -eq $default_config_path) {
    $bool_input = Read-Host "Do you want to write your configs to ${user_config_path}? ['true','false']"
    try {
        $config_write_back = [System.Convert]::ToBoolean($bool_input) 
    } catch [FormatException] {
        $config_write_back = $false
    }
    if ($config_write_back){
        $config_file_path = $user_config_path
    }
}
Write-Host "Writing configs (back) to $config_file_path"
$config_obj | ConvertTo-Json -depth 4 | Out-File $config_file_path

Write-Host "Written config: `n$config_obj `n   Distros:$($config_obj.Distros) `n   Additional Chocolatey Packages:$($config_obj.AdditionalChoco) `n   Additional Ansible Playbooks:$($config_obj.AdditionalPlaybooks) `n   Additional VSCode Extensions:$($config_obj.VSCodeExtensions) `n   VSCode Settings:$($config_obj.VSCodeSettings)"
# exit

# $($username) = $config_obj.Username
# TODO
# $shared_path_win = ''
# $shared_path_wsl = ''


$step++ ; Write-Host "`n --- Step ${step}: Install Chocolatey"
#  check if chocolatey is already installed
if (-not(Get-Command choco.exe -ErrorAction SilentlyContinue)) {
    Write-Output "Seems Chocolatey is not installed, installing it now"
    Set-ExecutionPolicy Bypass -Scope Process -Force; Invoke-Expression ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))
    Write-Host "Please reboot your system."
    exit
}
else {
    $testchoco = powershell choco -v
    Write-Output "Chocolatey Version $testchoco is already installed."
}
choco feature enable -name=exitOnRebootDetected
choco feature enable -n allowGlobalConfirmation

$step++ ; Write-Host "`n --- Step ${step}: Install WSL (Windows Subsystem for Linux)"
##
## Restart. May be needed. Hopefully not.
# Write-Host "Check if Microsoft-Windows-Subsystem-Linux is enabled"
# $wsl = Get-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
# if ( ! $wsl.State -eq 'Enabled') {
#     Write-Host "Changing RunOnce before script." -foregroundcolor "magenta"
#     $RunOnceKey = "HKLM:\Software\Microsoft\Windows\CurrentVersion\RunOnce"
#     set-itemproperty $RunOnceKey "NextRun" ('C:\Windows\System32\WindowsPowerShell\v1.0\Powershell.exe -executionPolicy Unrestricted -NoExit -File "$PSCommandPath"') # TODO
#     Write-Host "Need to enable Microsoft-Windows-Subsystem-Linux. Restart, when prompted."
#     Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
# }
# else {
#     Write-Host "Microsoft-Windows-Subsystem-Linux is enabled."
# }

## check if already installed
if ("$(choco info wsl --localonly)".Contains('1 packages installed')) {
    Write-Host "WSL is already installed. Nothing to do here."
}
else {
    choco upgrade wsl --yes
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Something went wrong. Exit."
        exit
    }
    Write-Host "Installed WSL. Please reboot."
    exit
}

$step++ ; Write-Host "`n --- Step ${step}: Install Ubuntu 18.04 in WSL"
##  check if already installed
if ("$(choco info wsl-ubuntu-1804 --localonly)".Contains('1 packages installed')) {
    Write-Host "WSL-Ubuntu-1804 is already installed. Nothing to do here."
}
else {
    choco upgrade wsl-ubuntu-1804 --yes
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Something went wrong. Exit."
        exit
    }
    Write-Host "Installed WSL Ubuntu 1804"

    ## Add to path 'C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped'
    #$env:Path += ";C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped"
    $envpath = (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path
    $envpath += "C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped;"
    Set-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH -Value $envpath
    (Get-ItemProperty -Path 'Registry::HKEY_LOCAL_MACHINE\System\CurrentControlSet\Control\Session Manager\Environment' -Name PATH).path.split(";")
    Write-Host "Added WSL Ubuntu 1804 to pathvariable"

    ## Change permissions
    $path = "C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped"
    $user = whoami
    $Rights = "FullControl"
    $InheritSettings = "Containerinherit, ObjectInherit" #"None"
    $PropogationSettings = "None"
    $RuleType = "Allow"
    $acl = Get-Acl $path
    $perm = $user, $Rights, $InheritSettings, $PropogationSettings, $RuleType
    $rule = New-Object -TypeName System.Security.AccessControl.FileSystemAccessRule -ArgumentList $perm
    $acl.SetAccessRule($rule)
    $acl | Set-Acl -Path $path
    Write-Host "Changed permissions for WSL Ubuntu 1804"

    # # Add desktop shortcut
    # $objShell = New-Object -ComObject ("WScript.Shell")
    # $objShortCut = $objShell.CreateShortcut("C:\Users\Public\Desktop\" + "\Ubuntu1804.lnk")
    # $objShortCut.TargetPath = "C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped\ubuntu1804.exe"
    # $objShortCut.Save()
    
    # Add startmenu shortcut
    $objShell = New-Object -ComObject ("WScript.Shell")
    $objShortCut = $objShell.CreateShortcut("$env:APPDATA\Microsoft\Windows\Start Menu\Programs" + "\Ubuntu1804.lnk")
    $objShortCut.TargetPath = "C:\ProgramData\chocolatey\lib\wsl-ubuntu-1804\tools\unzipped\ubuntu1804.exe"
    $objShortCut.Save()

    Write-Host "Add shortcuts for WSL Ubuntu 1804"


    Write-Host "`n Please reboot now."
    exit
}

# $distro_exe = 'ubuntu1804'
$distro = 'ubuntu1804'

$step++ ; Write-Host "`n --- Step ${step}: Prepare Ubuntu"
$distro_username = & $distro run whoami
if ("$distro_username" -eq "root") {
    Write-Host "Was already running as root"
}
else {    
    & $distro config --default-user root
    Write-Host "Set default user to root"
}

## TODO only on new install
## TODO move to ansible
& $distro run apt update --yes
& $distro run DEBIAN_FRONTEND=noninteractive apt full-upgrade --yes
& $distro run DEBIAN_FRONTEND=noninteractive apt autoremove --yes

$step++ ; Write-Host "`n --- Step ${step}: Setup shared path"
###
# TODO "automount" SD-Card
# TODO set up shared path

$step++ ; Write-Host "`n --- Step ${step}: Setup Ansible in Ubuntu"
# TODO check if ansibe is already installed
& $distro run apt-get install software-properties-common --yes
& $distro run apt-add-repository --yes --update ppa:ansible/ansible
& $distro run apt-get install ansible --yes

$step++ ; Write-Host "`n --- Step ${step}: Setup user in Ubuntu"
###
& $distro run ansible-playbook ubuntu_user_playbook.yaml --extra-vars "username=$($config_obj.Username)"

$step++ ; Write-Host "`n --- Step ${step}: Setup tools in WSL with Ansible"
## Maybe alias
###
& $distro run ansible-playbook ubuntu_app_playbook.yaml

$step++ ; Write-Host "`n --- Step ${step}: Run additional Ansible Playbooks"
###    
if ($config_obj.AdditionalPlaybooks.Count -ne 0) {
    foreach ($playbook in $config_obj.AdditionalPlaybooks)
        {
            Write-Host "Got additional playbook: $playbook"
            & $distro run ansible-playbook $playbook
        }
} else {
    Write-Host "Got no additional playbooks"
}

$step++ ; Write-Host "`n --- Step ${step}: Install Docker"
###
if ($config_obj.Docker){
# choco upgrade choco install docker-desktop --yes
iwr -useb https://chocolatey.org/install.ps1|iex
choco install -y -pre docker-desktop #--force

### TODO Expose Deamon on tcp://localhost:2375 without TLS

# $docker_configs_path = 'C:\ProgramData\Docker\config' # C:\Users\benar\.docker
# Write-Host "Writing configs (back) to 'C:\ProgramData\Docker\config\daemon.json'"
# if (!(Test-Path $docker_configs_path -PathType Container)) {
#     New-Item -ItemType Directory -Force -Path $docker_configs_path
# }
# $docker_config = $config_obj.DockerConfig
# $docker_config | ConvertTo-Json -depth 4 | Out-File "C:\ProgramData\Docker\config\daemon.json"
# Write-Host "Written config: `n$docker_config"

& $distro run ansible-playbook ubuntu_docker_playbook.yaml --extra-vars "username=$($config_obj.Username)"
} else {
    Write-Host "Nothing to do here."
}

$step++ ; Write-Host "`n --- Step ${step}: Set default user in Ubuntu"
# $($config_obj.Username)
if ("$distro_username" -eq "root") {
    & $distro config --default-user $($config_obj.Username)
    Write-Host "Set default user to new $($config_obj.Username)"
}
elseif ("$distro_username" -eq "$($config_obj.Username)") {    
    & $distro config --default-user $distro_username
    Write-Host "Set default user back to $distro_username"
}
elseif ("$distro_username" -ne "$($config_obj.Username)") {    
    & $distro config --default-user $($config_obj.Username)
    Write-Host "Given username $($config_obj.Username) is different than previous username $distro_username"
    Write-Host "Set default user to $($config_obj.Username)"
} 

$step++ ; Write-Host "`n --- Step ${step}: Install and setup VSCode on Windows"
## check if already installed
if ("$(choco info vscode --localonly)".Contains('1 packages installed')) {
    Write-Host "VScode is already installed. Maybe updating."
    choco upgrade vscode --params "/NoDesktopIcon" --yes
}
else {
    choco upgrade vscode --params "/NoDesktopIcon" --yes
    if ($LASTEXITCODE -ne 0) {
        Write-Host "Something went wrong. Exit."
        exit
    }
    Write-Host "Installed VScode. Please restart Powershell."
    exit
}

# ###
# choco upgrade vscode-gitlens --yes

# Write-Host "Enable WSL-Remote-Execution for VSCode" # TODO
# & $distro run code .
## TODO install local extentions in vscode server

$extensions_count = 0
foreach ($extension in $config_obj.VSCodeExtentions) 
{
    $extensions_count++ ; Write-Host "`nInstall VSCode extension Nr. ${extensions_count}: $extension `n   (More informations at: https://marketplace.visualstudio.com/items?itemName=${extension})"
    code --install-extension $extension --force 
}

# code --uninstall-extension ms-vscode.csharp
# code --list-extensions

$step++ ; Write-Host "`n --- Step ${step}: Update VSCode-Settings on Windows"
## Settings
# https://code.visualstudio.com/docs/getstarted/settings#_settings-file-locations
$user = "$env:UserName"
$vscode_settings_filename = "settings.json"
$vscode_settings_path = "C:\Users\${user}\AppData\Roaming\Code\User\${vscode_settings_filename}"
if (!(Test-Path $vscode_settings_path))
{
   New-Item -path "C:\Users\${user}\AppData\Roaming\Code\User" -name $vscode_settings_filename -type "file" -value "{}"
   Write-Host "Created new vscode settingsfile"
}
$settings_json= Get-Content $vscode_settings_path -raw | ConvertFrom-Json
$key_values = $config_obj.VSCodeSettings

$VSCodeSettingsType = $config_obj.VSCodeSettings.GetType()
$key_values_type = $key_values.GetType()

foreach($key in $key_values.Keys)
{
    $value = $key_values[$key] ; write-host "$key = $value"
    try {
        $settings.$key = "$value"
    
    } catch {
        $settings_json| add-member -Name "$key" -value "$value" -MemberType NoteProperty -Force
    }
}
$settings_json| ConvertTo-Json -depth 32| set-content $vscode_settings_path


$step++ ; Write-Host "`n --- Step ${step}: Install git"
###

if ($config_obj.WinGit){
    choco upgrade git --yes
    git config --system user.name "Benjamin Arnold"
    git config --system user.email "ben.arnold@outlook.de"

    # To use git, a new PowerShell session must be started
    Write-Host "To use git, a new PowerShell session must be started"

    # # TODO add ssh-key to gitlab
    # if (!(Test-Path '~/.ssh/id_rsa.pub') -And !(Test-Path '~/.ssh/id_rsa')){
    #     Write-Host "Press ENTER" #TODO
    #     ssh-keygen -t rsa -b 4096
    # } else {
    #     Write-Host "SSH-Key already exsists."
    # }
}

$step++ ; Write-Host "`n --- Step ${step}: Run additional Chocolatey packages"
###    
$packages_count = 0
if ($config_obj.AdditionalChoco.Count -ne 0) {
    foreach ($package in $config_obj.AdditionalChoco)
        {
            $packages_count++ ; Write-Host "Install additional Chocolatey package Nr. ${packages_count}: $package"
            choco upgrade $package --yes
        }
} else {
    Write-Host "Got no additional Chocolatey packages"
}

# $step++ ; Write-Host "`n --- Step ${step}: "
# ###

$step++ ; Write-Host "`n --- Step ${step}: Summary"
###
Write-Host "${step} steps have been gone."
Write-Host "${extensions_count} extensions have been installed/updated/checked."

choco list --localonly

Write-Host "Everything is done.`n"
$exit = Read-Host "Enter anything to exit"
Write-Host "You entered: '$exit'. Bye!`n"


# Set back
$Host.UI.RawUI.WindowTitle = $initial_window_title
$Host.UI.RawUI.BackgroundColor = $initial_background_color
# clear-host
Powershell
exit
