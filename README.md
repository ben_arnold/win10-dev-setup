# **WIP** Setup tools for developers on Windows

## TL;DR

- Download ´setup_bwp_with_choco.ps1´ to a local directory on your machine. The 'Downloads' folder works fine.
- Run ´setup_bwp_with_choco.ps1´ on PowerShell in elevated mode.
- If it asks you to reboot, reboot.
- Start ´setup_bwp_with_choco.ps1´ again, after reboot, untill it says, it's done. *(automation WIP)*
- **DO NOT RUN ON A SYSTEM, THAT'S ALREADY SET UP! THIS MAY F&%§ YOUR CONFIGS** *(fix WIP)*
